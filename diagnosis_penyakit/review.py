from .models import UserAnswer, DiagnosisPenyakit, Gejala, Penyakit

def reviewDiagnosis():
    diagnosis_penyakit = DiagnosisPenyakit.objects.all().order_by('-timestamp', '-number_diagnosis')
    num_diagnosis = []
    parameter = []
    user = []
    for diagnosis in diagnosis_penyakit:
        num_diagnosis.append(diagnosis.number_diagnosis)
        user.append(diagnosis.user_id)
    fix_user = []
    fix_num = []
    for i in range(0, len(num_diagnosis) - 1):
        if num_diagnosis[i] != num_diagnosis[i + 1]:
            fix_num.append(num_diagnosis[i])
            fix_user.append(user[i])
        if i + 1 == len(num_diagnosis) - 1:
            fix_num.append(num_diagnosis[i + 1])
            fix_user.append(user[i + 1])
    parameter.append(fix_user)
    parameter.append(fix_num)
    fix_point = []
    fix_penyakit = []
    point = []
    for i in range(0, len(fix_user)):
        for diagnosis in diagnosis_penyakit:
            if diagnosis.user_id == fix_user[i] and diagnosis.number_diagnosis == fix_num[i]:
                point.append(diagnosis.total_point)
        point.sort(reverse=True)
        fix_point.append(point[0])
        a = 0
        for diagnosis in diagnosis_penyakit:
            if a==0:
                if diagnosis.user_id == fix_user[i] and diagnosis.number_diagnosis == fix_num[i] and diagnosis.total_point==fix_point[i]:
                    penyakit = Penyakit.objects.all().filter(id = diagnosis.penyakit_id)
                    fix_penyakit.append(penyakit[0])
                    a=1
        point = []
    print(fix_point)
    parameter.append(fix_penyakit)
    gejala = []
    for i in range(0, len(fix_penyakit)):
        gejala_penyakit = Gejala.objects.all().filter(penyakit_id=fix_penyakit[i].id)
        gejala.append(gejala_penyakit)
    parameter.append(gejala)
    return parameter
