# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-04-19 04:15
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diagnosis_penyakit', '0019_auto_20180412_0909'),
    ]

    operations = [
        migrations.RenameField(
            model_name='review',
            old_name='id_ahli',
            new_name='ahli',
        ),
        migrations.RenameField(
            model_name='review',
            old_name='id_penyakit',
            new_name='penyakit',
        ),
    ]
