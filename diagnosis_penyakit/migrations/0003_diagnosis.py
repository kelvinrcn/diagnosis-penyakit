# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-01-24 05:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('diagnosis_penyakit', '0002_auto_20180123_2113'),
    ]

    operations = [
        migrations.CreateModel(
            name='Diagnosis',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_penyakit', models.IntegerField()),
                ('diagnosis', models.TextField(blank=True, null=True)),
            ],
        ),
    ]
