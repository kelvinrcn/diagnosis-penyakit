# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-19 02:48
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diagnosis_penyakit', '0042_reviewfromuser_number_diagnosis'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gejaladiagnosis',
            name='user',
        ),
        migrations.DeleteModel(
            name='GejalaDiagnosis',
        ),
    ]
