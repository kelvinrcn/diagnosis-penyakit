# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-01-27 04:09
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diagnosis_penyakit', '0005_gejala_id_organ'),
    ]

    operations = [
        migrations.RenameField(
            model_name='gejala',
            old_name='id_organ',
            new_name='organ',
        ),
    ]
