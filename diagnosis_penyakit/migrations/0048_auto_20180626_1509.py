# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-26 08:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('diagnosis_penyakit', '0047_auto_20180626_1508'),
    ]

    operations = [
        migrations.AlterField(
            model_name='learningpenyakit',
            name='ahli',
            field=models.IntegerField(),
        ),
    ]
