# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-16 02:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('diagnosis_penyakit', '0050_auto_20180706_1003'),
    ]

    operations = [
        migrations.AddField(
            model_name='diagnosispenyakit',
            name='status_learning',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='reviewdiagnosis',
            name='status_learning',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='reviewfromuser',
            name='status_learning',
            field=models.IntegerField(default=0),
        ),
    ]
