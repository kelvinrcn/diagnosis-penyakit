# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-18 04:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('diagnosis_penyakit', '0041_penyakit_bidang_ahli'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewfromuser',
            name='number_diagnosis',
            field=models.IntegerField(default=0),
        ),
    ]
