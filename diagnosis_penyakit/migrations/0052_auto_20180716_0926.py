# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-16 02:26
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diagnosis_penyakit', '0051_auto_20180716_0921'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reviewdiagnosis',
            name='status_learning',
        ),
        migrations.RemoveField(
            model_name='reviewfromuser',
            name='status_learning',
        ),
    ]
