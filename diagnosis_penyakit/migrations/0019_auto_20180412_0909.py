# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-04-12 02:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('diagnosis_penyakit', '0018_auto_20180219_1003'),
    ]

    operations = [
        migrations.CreateModel(
            name='AhliPenyakit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nip', models.CharField(max_length=30)),
                ('nama', models.CharField(max_length=250)),
                ('ahli_bidang', models.CharField(max_length=100)),
                ('lama_kerja', models.IntegerField(default=0)),
                ('jumlah_review_benar', models.IntegerField(default=0)),
                ('tempat_kerja', models.CharField(max_length=500)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number_diagnosis', models.IntegerField(default=0)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('id_ahli', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diagnosis_penyakit.AhliPenyakit')),
                ('id_penyakit', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diagnosis_penyakit.Penyakit')),
            ],
        ),
        migrations.AddField(
            model_name='useranswer',
            name='number_diagnosis',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='diagnosis',
            name='penyakit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diagnosis_penyakit.Penyakit'),
        ),
        migrations.AlterField(
            model_name='gejala',
            name='penyakit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diagnosis_penyakit.Penyakit'),
        ),
        migrations.AlterField(
            model_name='pencegahan',
            name='penyakit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diagnosis_penyakit.Penyakit'),
        ),
        migrations.AlterField(
            model_name='pengobatan',
            name='penyakit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diagnosis_penyakit.Penyakit'),
        ),
        migrations.AlterField(
            model_name='penyebab',
            name='penyakit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diagnosis_penyakit.Penyakit'),
        ),
    ]
