from django.shortcuts import render
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from .models import UserAnswer, Penyakit, DiagnosisPenyakit, Gejala, Penyebab, Diagnosis, Pengobatan, Pencegahan, \
    ReviewDiagnosis, AhliPenyakit, ReviewFromUser
from .matching import save_match, get_match, makePoint
from .form import UserResponseForm, UserResponseSearchForm, AhliResponseForm, ReviewUserForm
from .review import reviewDiagnosis
from .neural_network import makeModelNeuralNetwork, make_dataset, updateGejala, make_dataset_fix, makePrediction, \
    make_data_test
from django.db.models import Q
from django import forms


def home(request):
    if request.user.is_authenticated:
        form_review = ReviewUserForm(request.POST or None)
        form = UserResponseForm(request.POST or None)
        # dataset = make_dataset()
        # print(len(dataset), "test")
        # if len(dataset)>0:
        # 	if dataset[5]=='dataset':
        # 		dataset_fix = make_dataset_fix(dataset)
        # 		if dataset_fix!='sukses':
        # 			make_model = makeModelNeuralNetwork()
        # 			if make_model=='sukses':
        # 				print ('Model NN Berhasil dibuat')
        # 	if dataset[5]=='data_prediksi':
        # 		dataset_fix = make_dataset_fix(dataset)
        # 		if dataset_fix == 'sukses':
        # 			data_test = make_data_test()
        # 			prediction = makePrediction()
        # 			update_gejala = updateGejala(prediction)
        # 			if update_gejala=='sukses':
        # 				print ('Prediksi Sukses')
        list_rev_user = []
        rev_user = ReviewFromUser.objects.all().filter(user_id=request.user.id)
        for rev in rev_user:
            list_rev_user.append(rev.number_diagnosis)
        num_diagnosis = UserAnswer.objects.all().filter(user_id=request.user.id).order_by("-number_diagnosis")
        num = list_rev_user.count(num_diagnosis[0].number_diagnosis)
        if len(num_diagnosis) > 0:
            user_answer = UserAnswer.objects.all().filter(user_id=request.user.id,
                                                          number_diagnosis=num_diagnosis[0].number_diagnosis)
            diagnosis = DiagnosisPenyakit.objects.all().filter(user_id=request.user.id, number_diagnosis=num_diagnosis[
                0].number_diagnosis).order_by("-total_point")
            context = {
                "form": form,
                "form_review": form_review,
                "user_answer": user_answer,
                "diagnosis": diagnosis,
                "hasil_diagnosis": diagnosis[0],
                "num": num,
            }
            return render(request, "penyakit/home.html", context)
        else:
            context = {
                "form": form,
            }
            return render(request, "penyakit/home.html", context)
    else:
        raise Http404


def homeAhliPenyakit(request):
    if request.user.is_authenticated:
        form = AhliResponseForm(request.POST or None)
        user_answer = UserAnswer.objects.all().order_by('-timestamp')
        review = reviewDiagnosis()
        fix_user = review[0]
        diagnosis = []
        fix_num = review[1]
        fix_penyakit = review[2]
        gejala = review[3]
        for i in range(0, len(fix_user)):
            diagnosis_penyakit = DiagnosisPenyakit.objects.all().filter(number_diagnosis=fix_num[i],
                                                                        user_id=fix_user[i])[:10]
            diagnosis.append(diagnosis_penyakit)
        context = {
            "fix_num": fix_num,
            "gejala": gejala,
            "form": form,
            "fix_user": fix_user,
            "fix_penyakit": fix_penyakit,
            "user_answer": user_answer,
            "diagnosis": diagnosis[:10]
        }
        return render(request, "penyakit/home_ahli.html", context)
    else:
        raise Http404


def responsePenyakit(request):
    if request.user.is_authenticated:
        # form = UserResponseForm(request.POST or None)
        if request.method == 'POST':
            form = UserResponseForm(request.POST)
            gejala0 = request.POST.getlist('gejala_id0')
            gejala1 = form["gejala_id1"].data
            gejala2 = form["gejala_id2"].data
            gejala3 = form["gejala_id3"].data
            gejala4 = form["gejala_id4"].data
            gejala5 = form["gejala_id5"].data
            gejala6 = form["gejala_id6"].data
            gejala7 = form["gejala_id7"].data
            gejala8 = form["gejala_id8"].data
            gejala9 = form["gejala_id9"].data
            gejala10 = form["gejala_id10"].data
            gejala11 = form["gejala_id11"].data
            gejala12 = form["gejala_id12"].data
            gejala13 = form["gejala_id13"].data
            gejala14 = form["gejala_id14"].data
            gejala15 = form["gejala_id15"].data
            gejala16 = form["gejala_id16"].data
            gejala17 = form["gejala_id17"].data
            gejala18 = form["gejala_id18"].data
            gejala19 = form["gejala_id19"].data
            gejala20 = form["gejala_id20"].data
            gejala21 = form["gejala_id21"].data
            gejala22 = form["gejala_id22"].data
            gejala23 = form["gejala_id23"].data
            gejala24 = form["gejala_id24"].data
            gejala25 = form["gejala_id25"].data
            gejala26 = form["gejala_id26"].data
            gejala27 = form["gejala_id27"].data
            gejala28 = form["gejala_id28"].data
            gejala29 = form["gejala_id29"].data
            gejala30 = form["gejala_id30"].data
            gejala31 = form["gejala_id31"].data
            gejala32 = form["gejala_id32"].data
            gejala33 = form["gejala_id33"].data
            gejala34 = form["gejala_id34"].data
            gejala35 = form["gejala_id35"].data
            gejala36 = form["gejala_id36"].data
            gejala37 = form["gejala_id37"].data
            gejala38 = form["gejala_id38"].data
            gejala39 = form["gejala_id39"].data
            gejala40 = form["gejala_id40"].data
            gejala41 = form["gejala_id41"].data
            gejala42 = form["gejala_id42"].data
            gejala43 = form["gejala_id43"].data
            gejala44 = form["gejala_id44"].data
            gejala45 = form["gejala_id45"].data
            gejala46 = form["gejala_id46"].data
            gejala47 = form["gejala_id47"].data
            gejala48 = form["gejala_id48"].data
            gejala49 = form["gejala_id49"].data
            gejala50 = form["gejala_id50"].data
            gejala51 = form["gejala_id51"].data
            gejala52 = form["gejala_id52"].data
            gejala53 = form["gejala_id53"].data
            gejala54 = form["gejala_id54"].data
            gejala55 = form["gejala_id55"].data
            gejala56 = form["gejala_id56"].data
            gejala57 = form["gejala_id57"].data
            gejala58 = form["gejala_id58"].data
            gejala59 = form["gejala_id59"].data
            gejala60 = form["gejala_id60"].data
            gejala61 = form["gejala_id61"].data
            gejala62 = form["gejala_id62"].data
            gejala63 = form["gejala_id63"].data

    # if form.is_valid():
    # 	gejala1 = form["gejala_id1"].data('gejala_id1')
    # 	print(gejala1)
    # 	print('test123')
    # 	gejala2 = form["gejala_id1"].data('gejala_id2')
    # 	gejala3 = form["gejala_id1"].data('gejala_id3')
    # 	gejala4 = form["gejala_id1"].data('gejala_id4')
    # 	gejala5 = form["gejala_id1"].data('gejala_id5')
    # 	gejala6 = form["gejala_id1"].data('gejala_id6')
    # 	gejala7 = form["gejala_id1"].data('gejala_id7')
    # 	gejala8 = form["gejala_id1"].data('gejala_id8')
    # 	gejala9 = form["gejala_id1"].data('gejala_id9')
    # 	gejala10 = form["gejala_id1"].data('gejala_id10')
    # 	gejala11 = form["gejala_id1"].data('gejala_id11')
    # 	gejala12 = form["gejala_id1"].data('gejala_id12')
    # 	gejala13 = form["gejala_id1"].data('gejala_id13')
    # 	gejala14 = form["gejala_id1"].data('gejala_id14')
    # 	gejala15 = form["gejala_id1"].data('gejala_id15')
    # 	gejala16 = form["gejala_id1"].data('gejala_id16')
    # 	gejala17 = form["gejala_id1"].data('gejala_id17')
    # 	gejala18 = form["gejala_id1"].data('gejala_id18')
    # 	gejala19 = form["gejala_id1"].data('gejala_id19')
    # 	gejala20 = form["gejala_id1"].data('gejala_id20')
    # 	gejala21 = form["gejala_id1"].data('gejala_id21')
    # 	gejala22 = form["gejala_id1"].data('gejala_id22')
    # 	gejala23 = form["gejala_id1"].data('gejala_id23')
    # 	gejala24 = form["gejala_id1"].data('gejala_id24')
    # 	gejala25 = form["gejala_id1"].data('gejala_id25')
    # 	gejala26 = form["gejala_id1"].data('gejala_id26')
    # 	gejala27 = form["gejala_id1"].data('gejala_id27')
    # 	gejala28 = form["gejala_id1"].data('gejala_id28')
    # 	gejala29 = form["gejala_id1"].data('gejala_id29')
    # 	gejala30 = form["gejala_id1"].data('gejala_id30')
    # 	gejala31 = form["gejala_id1"].data('gejala_id31')
    # 	gejala32 = form["gejala_id1"].data('gejala_id32')
    # 	gejala33 = form["gejala_id1"].data('gejala_id33')
    # 	gejala34 = form["gejala_id1"].data('gejala_id34')
    # 	gejala35 = form["gejala_id1"].data('gejala_id35')
    # 	gejala36 = form["gejala_id1"].data('gejala_id36')
    # 	gejala37 = form["gejala_id1"].data('gejala_id37')
    # 	gejala38 = form["gejala_id1"].data('gejala_id38')
    # 	gejala39 = form["gejala_id1"].data('gejala_id39')
    # 	gejala40 = form["gejala_id1"].data('gejala_id40')
    # 	gejala41 = form["gejala_id1"].data('gejala_id41')
    # 	gejala42 = form["gejala_id1"].data('gejala_id42')
    # 	gejala43 = form["gejala_id1"].data('gejala_id43')
    # 	gejala44 = form["gejala_id1"].data('gejala_id44')
    # 	gejala45 = form["gejala_id1"].data('gejala_id45')
    # 	gejala46 = form["gejala_id1"].data('gejala_id46')
    # 	gejala47 = form["gejala_id1"].data('gejala_id47')
    # 	gejala48 = form["gejala_id1"].data('gejala_id48')
    # 	gejala49 = form["gejala_id1"].data('gejala_id49')
    # 	gejala50 = form["gejala_id1"].data('gejala_id50')
    # 	gejala51 = form["gejala_id1"].data('gejala_id51')
    # 	gejala52 = form["gejala_id1"].data('gejala_id52')
    # 	gejala53 = form["gejala_id1"].data('gejala_id53')
    # 	gejala54 = form["gejala_id1"].data('gejala_id54')
    # 	gejala55 = form["gejala_id1"].data('gejala_id55')
    # 	gejala56 = form["gejala_id1"].data('gejala_id56')
    # 	gejala57 = form["gejala_id1"].data('gejala_id57')
    # 	gejala58 = form["gejala_id1"].data('gejala_id58')
    # 	gejala59 = form["gejala_id1"].data('gejala_id59')
    # 	gejala60 = form["gejala_id1"].data('gejala_id60')
    # 	gejala61 = form["gejala_id1"].data('gejala_id61')
    # 	gejala62 = form["gejala_id1"].data('gejala_id62')
    # 	gejala63 = form["gejala_id1"].data('gejala_id63')

            answer = UserAnswer.objects.all().filter(user_id=request.user.id).order_by('-number_diagnosis')
            user = UserAnswer()
            if (len(answer) > 0):
                user = answer[1]
            else:
                user.number_diagnosis = 0

            # print(gejala0, "diluar if")
            if (len(gejala0) > 0):
                for i in range(0, len(gejala0)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala0[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()
                    # print(gejala0, "didalam if")

            if (len(gejala1) > 0):
                for i in range(0, len(gejala1)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala1[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala2) > 0):
                for i in range(0, len(gejala2)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala2[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala3) > 0):
                for i in range(0, len(gejala3)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala3[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala4) > 0):
                for i in range(0, len(gejala4)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala4[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala5) > 0):
                for i in range(0, len(gejala5)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala5[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala6) > 0):
                for i in range(0, len(gejala6)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala6[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala7) > 0):
                for i in range(0, len(gejala7)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala7[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala8) > 0):
                for i in range(0, len(gejala8)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala8[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala9) > 0):
                for i in range(0, len(gejala9)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala9[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala10) > 0):
                for i in range(0, len(gejala10)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala10[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala11) > 0):
                for i in range(0, len(gejala11)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala11[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala12) > 0):
                for i in range(0, len(gejala12)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala12[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala13) > 0):
                for i in range(0, len(gejala13)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala13[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala14) > 0):
                for i in range(0, len(gejala14)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala14[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala15) > 0):
                for i in range(0, len(gejala15)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala15[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala16) > 0):
                for i in range(0, len(gejala16)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala16[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala17) > 0):
                for i in range(0, len(gejala17)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala17[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala18) > 0):
                for i in range(0, len(gejala18)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala18[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala19) > 0):
                for i in range(0, len(gejala19)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala19[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala20) > 0):
                for i in range(0, len(gejala20)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala20[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala21) > 0):
                for i in range(0, len(gejala21)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala21[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala22) > 0):
                for i in range(0, len(gejala22)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala22[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala23) > 0):
                for i in range(0, len(gejala23)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala23[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala24) > 0):
                for i in range(0, len(gejala24)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala24[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala25) > 0):
                for i in range(0, len(gejala25)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala25[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala26) > 0):
                for i in range(0, len(gejala26)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala26[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala27) > 0):
                for i in range(0, len(gejala27)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala27[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala28) > 0):
                for i in range(0, len(gejala28)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala28[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala29) > 0):
                for i in range(0, len(gejala29)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala29[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala30) > 0):
                for i in range(0, len(gejala30)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala30[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala31) > 0):
                for i in range(0, len(gejala31)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala31[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala32) > 0):
                for i in range(0, len(gejala32)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala32[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala33) > 0):
                for i in range(0, len(gejala33)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala33[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala34) > 0):
                for i in range(0, len(gejala34)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala34[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala35) > 0):
                for i in range(0, len(gejala35)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala35[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala36) > 0):
                for i in range(0, len(gejala36)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala36[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala37) > 0):
                for i in range(0, len(gejala37)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala37[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala38) > 0):
                for i in range(0, len(gejala38)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala38[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala39) > 0):
                for i in range(0, len(gejala39)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala39[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala40) > 0):
                for i in range(0, len(gejala40)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala40[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala41) > 0):
                for i in range(0, len(gejala41)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala41[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala42) > 0):
                for i in range(0, len(gejala42)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala42[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala43) > 0):
                for i in range(0, len(gejala43)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala43[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala44) > 0):
                for i in range(0, len(gejala44)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala44[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala45) > 0):
                for i in range(0, len(gejala45)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala45[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala46) > 0):
                for i in range(0, len(gejala46)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala46[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala47) > 0):
                for i in range(0, len(gejala47)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala47[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala48) > 0):
                for i in range(0, len(gejala48)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala48[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala49) > 0):
                for i in range(0, len(gejala49)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala49[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala50) > 0):
                for i in range(0, len(gejala50)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala50[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala51) > 0):
                for i in range(0, len(gejala51)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala51[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala52) > 0):
                for i in range(0, len(gejala52)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala52[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala53) > 0):
                for i in range(0, len(gejala53)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala53[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala54) > 0):
                for i in range(0, len(gejala54)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala54[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala55) > 0):
                for i in range(0, len(gejala55)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala55[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala56) > 0):
                for i in range(0, len(gejala56)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala56[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala57) > 0):
                for i in range(0, len(gejala57)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala57[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala58) > 0):
                for i in range(0, len(gejala58)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala58[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala59) > 0):
                for i in range(0, len(gejala59)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala59[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala60) > 0):
                for i in range(0, len(gejala60)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala60[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala61) > 0):
                for i in range(0, len(gejala61)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala61[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala62) > 0):
                for i in range(0, len(gejala62)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala62[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()

            if (len(gejala63) > 0):
                for i in range(0, len(gejala63)):
                    userAnswer = UserAnswer()
                    userAnswer.gejala_answer = gejala63[i]
                    userAnswer.user_id = request.user.id
                    userAnswer.number_diagnosis = user.number_diagnosis + 1
                    userAnswer.save()


            return redirect('diagnosis_penyakit:response_matching')

        #     print(request.POST.get('gejala_id'), "request")
        #     if 'gejala_id0' in request.POST:
        #         print("test")
        #         gejala0 = request.POST.get('gejala_id0')
        #         print(gejala0)
        #
        #         answer = UserAnswer.objects.all().filter(user_id=request.user.id).order_by('-number_diagnosis')
        #         user = UserAnswer()
        #         if (len(answer) > 0):
        #             user = answer[1]
        #         else:
        #             user.number_diagnosis = 0
        #
        #         if (len(gejala0) > 0):
        #             for i in range(0, len(gejala0)):
        #                 userAnswer = UserAnswer()
        #                 userAnswer.gejala_answer = gejala1[i]
        #                 userAnswer.user_id = request.user.id
        #                 userAnswer.number_diagnosis = user.number_diagnosis + 1
        #                 userAnswer.save()
        #                 print(gejala0)
        #
        # return redirect('diagnosis_penyakit:response_matching')
    else:
        raise Http404


def responseMatching(request):
    if request.user.is_authenticated:
        answer = UserAnswer.objects.all().filter(user_id=request.user.id).order_by("-number_diagnosis")
        userAnswer = UserAnswer.objects.all().filter(user_id=request.user.id,
                                                     number_diagnosis=answer[0].number_diagnosis)
        gejalaAnswer = []
        fix_point = []
        for i in range(0, len(userAnswer)):
            gejalaAnswer.append(userAnswer[i].gejala_answer)

        gejalaMatch = get_match(gejalaAnswer)
        point = makePoint(gejalaMatch)
        for i in range(0, len(point)):
            num = 0
            gejala = Gejala.objects.all().filter(penyakit_id=point[i][0])
            for gejala1 in gejala:
                num = num + 1
            print("Num", num)
            print(point[i][1])
            hasil = int((point[i][1] / num) * 100)
            fix_point.append([point[i][0], hasil])
            print ("Hasil :", hasil, "%")
        hasil = save_match(request.user, fix_point)
        if (hasil == 'sukses'):
            fix = []
            diagnosis = DiagnosisPenyakit.objects.all().filter(user_id=request.user.id).order_by("-number_diagnosis")
            penyakit = DiagnosisPenyakit.objects.all().filter(user_id=request.user.id,
                                                              number_diagnosis=diagnosis[0].number_diagnosis).order_by(
                '-total_point')
            for i in range(0, len(penyakit)):
                if penyakit[i].total_point > 0:
                    fix.append(penyakit[i])
                else:
                    break
            context = {
                "diagnosis": fix[:10],
            }
            return render(request, "penyakit/penyakit_response.html", context)
        return render(request, "penyakit/penyakit_response.html")
    else:
        raise Http404


def detailPenyakit(request, id):
    if request.user.is_authenticated:
        penyakit = Penyakit.objects.get(id=id)
        penyebab = Penyebab.objects.filter(penyakit_id=id)
        gejala = Gejala.objects.filter(penyakit_id=id)
        diagnosis = Diagnosis.objects.filter(penyakit_id=id)
        pengobatan = Pengobatan.objects.filter(penyakit_id=id)
        pencegahan = Pencegahan.objects.filter(penyakit_id=id)

        context = {
            'penyakit': penyakit,
            'penyebab': penyebab,
            'gejala': gejala,
            'diagnosis': diagnosis,
            'pengobatan': pengobatan,
            'pencegahan': pencegahan,
        }

        return render(request, "penyakit/penyakit_detail.html", context)
    else:
        raise Http404


def showDiagnosisResult(request):
    if request.user.is_authenticated:
        user_answer = UserAnswer.objects.all().order_by('-number_diagnosis')
        diagnosis_penyakit = DiagnosisPenyakit.objects.all().order_by('-number_diagnosis')
        context = {
            'user_answer': user_answer,
            'diagnosis_penyakit': diagnosis_penyakit
        }

        return render(request, "penyakit/home_ahli.html", context)
    else:
        raise Http404


def reviewGejala(request, id):
    if request.user.is_authenticated:
        gejala = Gejala.objects.all().filter(penyakit_id=id)
        penyakit = Penyakit.objects.all().filter(id=id)
        context = {
            'gejala_penyakit': gejala,
            'penyakit': penyakit[0],
        }
        return render(request, "penyakit/tambah_penyakit.html", context)
    else:
        raise Http404


def saveReviewGejala(request):
    if request.user.is_authenticated:
        gejala_review = form["gejala_id1"].datalist('gejala')
        poin_review = form["gejala_id1"].datalist('poin')
        penyakit = form["gejala_id1"].datalist('penyakit')
        penyakit1 = Penyakit.objects.all().filter(nama_penyakit=penyakit[0])
        for i in range(0, len(gejala_review)):
            if gejala_review[i] != '' and poin_review[i] != '':
                review = ReviewGejala()
                review.ahli = request.user
                review.penyakit_id = penyakit1[0].id
                review.gejala = gejala_review[i]
                review.poin = poin_review[i]
                review.save()

        return redirect('diagnosis_penyakit:home_ahli')
    else:
        raise Http404


def allPenyakit(request):
    if request.user.is_authenticated:
        penyakit = Penyakit.objects.all()
        form_review = ReviewUserForm(request.POST or None)
        penyakit1 = []
        penyakit2 = []
        penyakit3 = []
        penyakit4 = []
        for i in range(0, len(penyakit)):
            if i <= 55:
                penyakit1.append(penyakit[i])
            elif i > 55 and i <= 110:
                penyakit2.append(penyakit[i])
            elif i > 110 and i <= 165:
                penyakit3.append(penyakit[i])
            elif i > 165 and i <= 226:
                penyakit4.append(penyakit[i])

        context = {
            'penyakit1': penyakit1,
            'penyakit2': penyakit2,
            'penyakit3': penyakit3,
            'penyakit4': penyakit4,
            'form_review': form_review,
        }
        return render(request, "penyakit/all_penyakit_review.html", context)
    else:
        raise Http404


def tambahPenyakit(request, id):
    if request.user.is_authenticated:
        penyakit = Penyakit.objects.all()
        penyakit1 = []
        penyakit2 = []
        penyakit3 = []
        penyakit4 = []
        for i in range(0, len(penyakit)):
            if i <= 55:
                penyakit1.append(penyakit[i])
            elif i > 55 and i <= 110:
                penyakit2.append(penyakit[i])
            elif i > 110 and i <= 165:
                penyakit3.append(penyakit[i])
            elif i > 165 and i <= 226:
                penyakit4.append(penyakit[i])

        review = reviewDiagnosis()
        fix_user = review[0]
        fix_num = review[1]
        fix_user = fix_user[int(id)]
        fix_num = fix_num[int(id)]
        context = {
            'penyakit1': penyakit1,
            'penyakit2': penyakit2,
            'penyakit3': penyakit3,
            'penyakit4': penyakit4,
            "number_diagnosis": fix_num,
            "user_id": fix_user,
        }
        return render(request, "penyakit/review_penyakit.html", context)
    else:
        raise Http404


def saveReviewFromUser(request):
    if request.user.is_authenticated:
        form = ReviewUserForm(request.POST or None)
        answer = UserAnswer.objects.all().filter(user_id=request.user.id).order_by('-number_diagnosis')
        if form.is_valid():
            review = form.save(commit=False)
            review.user = request.user
            penyakit = []
            for i in range(0, 238):
                gejala_review = request.POST.getlist(str(i))
                if len(gejala_review) == 3:
                    penyakit.append(gejala_review[0])

            review.penyakit_id = penyakit[0]
            review.number_diagnosis = answer[0].number_diagnosis
            review.save()
            status = 1
            context = {
                'status': status
            }
            return render(request, "penyakit/home.html", context)
        else:
            print(form.errors)
    else:
        raise Http404


def saveReviewPenyakit(request):
    if request.user.is_authenticated:
        penyakit = []
        for i in range(0, 238):
            gejala_review = request.POST.getlist(str(i))
            if len(gejala_review) == 3:
                penyakit.append(gejala_review[0])
                penyakit.append(gejala_review[1])
                penyakit.append(gejala_review[2])
        reviewDiagnosis = ReviewDiagnosis()
        reviewDiagnosis.penyakit_id = penyakit[0]
        reviewDiagnosis.user = penyakit[1]
        reviewDiagnosis.ahli = request.user
        reviewDiagnosis.number_diagnosis = penyakit[2]
        reviewDiagnosis.save()
        return redirect('diagnosis_penyakit:home_ahli')
    else:
        raise Http404


def allDiagnosisPasien(request):
    if request.user.is_authenticated:
        list_rev_user = []
        form_review = ReviewUserForm(request.POST or None)
        num_diagnosis = UserAnswer.objects.all().filter(user_id=request.user.id).order_by("-number_diagnosis")
        rev_user = ReviewFromUser.objects.all().filter(user_id=request.user.id)
        for rev in rev_user:
            list_rev_user.append(rev.number_diagnosis)
        list_num = []
        for num in num_diagnosis:
            if num.number_diagnosis not in list_num:
                list_num.append(num.number_diagnosis)

        if len(num_diagnosis) > 0:
            list_diagnosis = []
            list_hasil = []
            list_user_answer = []
            list = []
            for i in range(0, len(list_num)):
                user_answer = UserAnswer.objects.all().filter(user_id=request.user.id, number_diagnosis=list_num[i])
                diagnosis = DiagnosisPenyakit.objects.all().filter(user_id=request.user.id,
                                                                   number_diagnosis=list_num[i]).order_by(
                    "-total_point")
                list_diagnosis.append(diagnosis)
                list_hasil.append(diagnosis[0].penyakit)
                list_user_answer.append(user_answer)
                list.append(diagnosis[0].number_diagnosis)
            review = []
            for i in range(0, len(list[:10])):
                sum = list_rev_user.count(list[i])
                if sum == 1:
                    review.append(sum)
                else:
                    review.append(0)
            context = {
                "user_answer": list_user_answer,
                "form_review": form_review,
                "list_diagnosis1": list_diagnosis[:10],
                "hasil_diagnosis": list_hasil[:10],
                "review": review,
            }
            return render(request, "penyakit/hasil_diagnosis.html", context)
    else:
        raise Http404


def search(request):
    template = 'penyakit/home.html'

    query = request.GET.get('q')

    queryset = Gejala.objects.all().values_list('gejala', flat=True).distinct().filter(id_organ=1,
                                                                                       gejala__icontains=query).order_by(
        'gejala')

    # print(queryset, "search test")

    if request.user.is_authenticated:
        form_review = ReviewUserForm(request.POST or None)
        form = UserResponseSearchForm(request.POST or None, qry=query)
        # dataset = make_dataset()
        # print(len(dataset), "test")
        # if len(dataset)>0:
        # 	if dataset[5]=='dataset':
        # 		dataset_fix = make_dataset_fix(dataset)
        # 		if dataset_fix!='sukses':
        # 			make_model = makeModelNeuralNetwork()
        # 			if make_model=='sukses':
        # 				print ('Model NN Berhasil dibuat')
        # 	if dataset[5]=='data_prediksi':
        # 		dataset_fix = make_dataset_fix(dataset)
        # 		if dataset_fix == 'sukses':
        # 			data_test = make_data_test()
        # 			prediction = makePrediction()
        # 			update_gejala = updateGejala(prediction)
        # 			if update_gejala=='sukses':
        # 				print ('Prediksi Sukses')
        list_rev_user = []
        rev_user = ReviewFromUser.objects.all().filter(user_id=request.user.id)
        for rev in rev_user:
            list_rev_user.append(rev.number_diagnosis)
        num_diagnosis = UserAnswer.objects.all().filter(user_id=request.user.id).order_by("-number_diagnosis")
        num = list_rev_user.count(num_diagnosis[0].number_diagnosis)
        if len(num_diagnosis) > 0:
            user_answer = UserAnswer.objects.all().filter(user_id=request.user.id,
                                                          number_diagnosis=num_diagnosis[0].number_diagnosis)
            diagnosis = DiagnosisPenyakit.objects.all().filter(user_id=request.user.id, number_diagnosis=num_diagnosis[
                0].number_diagnosis).order_by("-total_point")
            context = {
                "queryset": queryset,
                "form": form,
                "form_review": form_review,
                "user_answer": user_answer,
                "diagnosis": diagnosis,
                "hasil_diagnosis": diagnosis[0],
                "num": num,
                "query": query,
            }
            return render(request, "penyakit/home.html", context)
        else:
            context = {
                "form": form,
            }
            return render(request, "penyakit/home.html", context)
    else:
        raise Http404

    return render_to_response(request, template, context)
