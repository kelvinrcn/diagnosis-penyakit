from django.apps import AppConfig


class DiagnosisPenyakitConfig(AppConfig):
    name = 'diagnosis_penyakit'
