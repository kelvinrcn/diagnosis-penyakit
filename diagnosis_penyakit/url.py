from django.conf.urls import url, include
from . import views
from .views import search

app_name = 'diagnosis_penyakit'

urlpatterns = [
    url(r'^$',views.home, name='home'),
    url(r'^results/$', views.search, name='search'),
    url(r'^ahli/',views.homeAhliPenyakit, name='home_ahli'),
    url(r'^response/matching', views.responseMatching , name='response_matching'),
    url(r'^response/', views.responsePenyakit , name='response'),
    url(r'^penyakit/detail/(?P<id>\d+)/$', views.detailPenyakit, name='detail_penyakit'),
    url(r'^gejala/review/(?P<id>\d+)/$', views.reviewGejala, name='review_gejala'),
    url(r'^gejala/review/save', views.saveReviewGejala, name='save_review_gejala'),
    url(r'^penyakit/all/(?P<id>\d+)/$', views.tambahPenyakit, name='tambah_penyakit'),
    url(r'^review/penyakit/all', views.allPenyakit, name='all_penyakit'),
    url(r'^user/diagnosis/all', views.allDiagnosisPasien, name='all_diagnosis_user'),
    url(r'^penyakit/review/save', views.saveReviewPenyakit, name='save_review_penyakit'),
    url(r'^penyakit/user/review/save', views.saveReviewFromUser, name='save_review_user'),
]