import os
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
from keras.models import Sequential
from keras.layers import Dense
from .models import ReviewDiagnosis, ReviewFromUser, AhliPenyakit, DiagnosisPenyakit, Penyakit, UserAnswer, Gejala
from datetime import datetime
from utama.models import Akun
from keras.models import model_from_json

def make_dataset():
    list_cek = []
    list_user_num = []
    diagnosisPenyakit = DiagnosisPenyakit.objects.all().filter(status_learning=0)
    for diagnosis in diagnosisPenyakit:
        list = [diagnosis.user.id, diagnosis.number_diagnosis]
        if list not in list_user_num:
            list_user_num.append(list)
    list_penyakit = []
    list1 = []
    for i in range(0, len(list_user_num)):
        diagnosis = DiagnosisPenyakit.objects.all().filter(user = list_user_num[i][0], number_diagnosis=list_user_num[i][1]).order_by('-total_point')
        list_penyakit.append(diagnosis[0].penyakit_id)
        if diagnosis[0].penyakit_id not in list1:
            list1.append(diagnosis[0].penyakit_id)

    list_jumlah_penyakit = []
    for i in range(0, len(list1)):
        num = list_penyakit.count(list1[i])
        list_jumlah_penyakit.append([list1[i], num])
    list2 = []
    for i in range(0, len(list_jumlah_penyakit)):
        if list_jumlah_penyakit[i][1] >= 5:
            list2.append([list_jumlah_penyakit[i][0], list_jumlah_penyakit[i][1]])
    if len(list2)>0:
        list_review = []
        rev_user = []
        for i in range(0, len(list2)):
            review = ReviewDiagnosis.objects.all().filter(penyakit_id=list2[i][0])
            if len(review)>=5:
                for rev in review:
                    list_review.append(rev)
                revUser = ReviewFromUser.objects.all().filter(penyakit_id=list2[i][0])
                for user in revUser:
                    rev_user.append([user.number_diagnosis, user.user_id, user.penyakit_id])
        if len(list_review)>0:
            list_user = []
            list_penyakit_fix = []
            for i in range(0, len(list_review)):
                list3 = [list_review[i].number_diagnosis, list_review[i].user, list_review[i].penyakit_id]
                if list3 not in list_user:
                    list_user.append(list3)
                if list_review[i].penyakit_id not in list_penyakit_fix:
                    list_penyakit_fix.append(list_review[i].penyakit_id)
            gejala_fix = []
            dataset = []
            data = os.path.join(os.getcwd(), 'diagnosis_penyakit\\neural_network\\dataset.xlsx')
            dataset3 = pd.read_excel(data, delimiter=',', encoding='latin-1', index_col=False)
            cek_penyakit = dataset3.iloc[:, 1].values
            cek=[]
            if len(dataset3)>0:
                for i in range(0, len(cek_penyakit)):
                    if cek_penyakit[i] not in cek:
                        cek.append(cek_penyakit[i])
            status = 0
            for i in range(0 ,len(cek)):
                for j in range(0, len(list_penyakit_fix)):
                    penyakit = Penyakit.objects.all().filter(id = list_penyakit_fix[j])
                    if cek[i]==penyakit[0].nama_penyakit:
                        status=1
            if status==0:
                for i in range(0, len(list_penyakit_fix)):
                    jlh_user = 0
                    for j in range(0, len(list_user)):
                        if list_penyakit_fix[i]==list_user[j][2]:
                            jlh_user=jlh_user+1
                    for j in range(0, len(list_user)):
                        if list_penyakit_fix[i]==list_user[j][2]:
                            userAnswer = UserAnswer.objects.all().filter(user_id=list_user[j][1], number_diagnosis=list_user[j][0])
                            for answer in userAnswer:
                                gejala_fix.append([answer.gejala_answer, list_penyakit_fix[i]])
                gejala_distict = []
                for i in range(0, len(gejala_fix)):
                    if gejala_fix[i] not in gejala_distict:
                        gejala_distict.append(gejala_fix[i])
                for i in range(0, len(gejala_distict)):
                    sum = 0
                    for j in range(0, len(gejala_fix)):
                        if gejala_distict[i]==gejala_fix[j]:
                            sum = sum+1
                    dataset.append([gejala_distict[i][0], gejala_distict[i][1],sum])
                list_fix = []
                list_fix.append(gejala_fix)
                list_fix.append(dataset)
                list_fix.append(list2)
                list_fix.append(list_penyakit_fix)
                list_fix.append(rev_user)
                list_fix.append('dataset')
                return list_fix
            elif len(list_penyakit_fix) == 1 and status==1:
                jlh_user = 0
                for j in range(0, len(list_user)):
                    if list_penyakit_fix[0]==list_user[j][2]:
                        jlh_user=jlh_user+1
                for j in range(0, len(list_user)):
                    if list_penyakit_fix[0]==list_user[j][2]:
                        userAnswer = UserAnswer.objects.all().filter(user_id=list_user[j][1], number_diagnosis=list_user[j][0])
                        for answer in userAnswer:
                            gejala_fix.append([answer.gejala_answer, list_penyakit_fix[0]])
                gejala_distict = []
                for i in range(0, len(gejala_fix)):
                    if gejala_fix[i] not in gejala_distict:
                        gejala_distict.append(gejala_fix[i])
                for i in range(0, len(gejala_distict)):
                    sum = 0
                    for j in range(0, len(gejala_fix)):
                        if gejala_distict[i]==gejala_fix[j]:
                            sum = sum+1
                    dataset.append([gejala_distict[i][0], gejala_distict[i][1],sum])
                list_fix = []
                list_fix.append(gejala_fix)
                list_fix.append(dataset)
                list_fix.append(list2)
                list_fix.append(list_penyakit_fix)
                list_fix.append(rev_user)
                list_fix.append('data_prediksi')
                return list_fix
    return list_cek

def make_dataset_fix(list):
    if len(list)>0:
        dataset = list[1]
        list_user = list[2]
        status_gejala = []
        for i in range(0, len(dataset)):
            all_gejala = Gejala.objects.all().filter(gejala=dataset[i][0], penyakit_id=dataset[i][1])
            if len(all_gejala)>0:
                for j in range(0, len(list_user)):
                    if dataset[i][1]==list_user[j][0]:
                        if dataset[i][2]==list_user[j][1]:
                            status_gejala.append([dataset[i][0], dataset[i][1], 'Tetap', dataset[i][2]])
                        else:
                            status_gejala.append([dataset[i][0], dataset[i][1], 'Berkurang', dataset[i][2]])
            else:
                status_gejala.append([dataset[i][0], dataset[i][1], 'Bertambah', dataset[i][2]])
        validasi_dokter = []
        num_diagnosis_user = []
        for i in range(0, len(status_gejala)):
            user_answer = UserAnswer.objects.all().filter(gejala_answer=status_gejala[i][0])
            for user in user_answer:
                jumlah_spesialis = 0
                review = ReviewDiagnosis.objects.all().filter(number_diagnosis=user.number_diagnosis, user=user.user_id, penyakit_id = status_gejala[i][1])
                if len(review)>0:
                    data = [user.number_diagnosis, user.user_id]
                    if data not in num_diagnosis_user:
                        num_diagnosis_user.append(data)
                rev_user = ReviewFromUser.objects.all().filter(number_diagnosis=user.number_diagnosis, user_id=user.user_id, penyakit_id = status_gejala[i][1])
                for rev in review:
                    akun = Akun.objects.all().filter(user_id=rev.ahli_id)
                    ahli = AhliPenyakit.objects.all().filter(akun_id=akun[0].id)
                    if ahli[0].ahli_bidang != 'Dokter Umum':
                        jumlah_spesialis = jumlah_spesialis + 1
                validasi_dokter.append([status_gejala[i][0],status_gejala[i][1], status_gejala[i][2], status_gejala[i][3], len(review), len(rev_user), jumlah_spesialis])
        fix_dataset = []
        for i in range(0, len(status_gejala)):
            jumlah_spesialis=0
            jumlah_dokter = 0
            jumlah_val_user = 0
            for j in range(0, len(validasi_dokter)):
                if status_gejala[i][0]==validasi_dokter[j][0] and status_gejala[i][1]==validasi_dokter[j][1]:
                    jumlah_spesialis = jumlah_spesialis+validasi_dokter[j][6]
                    jumlah_dokter = jumlah_dokter+validasi_dokter[j][4]
                    jumlah_val_user = jumlah_val_user+validasi_dokter[j][5]
            penyakit = Penyakit.objects.all().filter(id=status_gejala[i][1])
            fix_dataset.append([status_gejala[i][0],penyakit[0].nama_penyakit, status_gejala[i][2], status_gejala[i][3],jumlah_dokter, jumlah_val_user, jumlah_spesialis])
        for i in range(0, len(num_diagnosis_user)):
            DiagnosisPenyakit.objects.filter(number_diagnosis=num_diagnosis_user[i][0], user_id=num_diagnosis_user[i][1]).update(status_learning=1)
        if len(dataset)==0:
            data = pd.DataFrame(fix_dataset)
            data.to_excel('diagnosis_penyakit\\neural_network\\dataset.xlsx', index=False)
            for i in range(0, len(num_diagnosis_user)):
                DiagnosisPenyakit.objects.filter(number_diagnosis=num_diagnosis_user[i][0], user_id=num_diagnosis_user[i][1]).update(status_learning=1)
            return 'sukses'
        if len(dataset) > 0 and list[5] == 'dataset':
            data = pd.DataFrame(fix_dataset)
            data.to_excel('diagnosis_penyakit\\neural_network\\dataset2.xlsx', index=False)
            data = os.path.join(os.getcwd(), 'diagnosis_penyakit\\neural_network\\dataset.xlsx')
            dataset = pd.read_excel(data, delimiter=',', encoding='latin-1', index_col=False)
            return 'sukses'
        if len(dataset)>0 and list[5]=='data_prediksi':
            data = pd.DataFrame(fix_dataset)
            data.to_excel('diagnosis_penyakit\\neural_network\\databaru.xlsx', index=False)
            for i in range(0, len(num_diagnosis_user)):
                DiagnosisPenyakit.objects.filter(number_diagnosis=num_diagnosis_user[i][0], user_id=num_diagnosis_user[i][1]).update(status_learning=1)
            return 'sukses'
        return  'sukses'

def makeModelNeuralNetwork():
    # data preprocessing
    data = os.path.join(os.getcwd(), 'diagnosis_penyakit\\neural_network\\dataset.xlsx')
    dataset = pd.read_excel(data, delimiter=',', encoding='latin-1', index_col=False)
    X = dataset.iloc[:, 1:7].values
    y = dataset.iloc[:, 7].values
    # label encoder
    label = LabelEncoder()
    X[:, 0] = label.fit_transform(X[:, 0])
    X[:, 1] = label.fit_transform(X[:, 1])

    # Onehot Encoder
    list = []
    for i in range(0, len(X)):
        if X[i][0] not in list:
            list.append(X[i][0])
    if len(list)==2:
        onehotencoder = OneHotEncoder(categorical_features=[1])
        X = onehotencoder.fit_transform(X).toarray()
    else:
        onehotencoder = OneHotEncoder(categorical_features=[0])
        X = onehotencoder.fit_transform(X).toarray()
        onehotencoder = OneHotEncoder(categorical_features=[len(list)])
        X = onehotencoder.fit_transform(X).toarray()

    y = label.fit_transform(y)
    # normalisasi
    X = preprocessing.minmax_scale(X, feature_range=(0, 1), axis=0, copy=True)
    # neural network implementations
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    model = Sequential()
    if len(list)==2:
        model.add(Dense(units=50, input_dim=8, activation='relu'))
    else:
        model.add(Dense(units=50, input_dim=7+len(list), activation='relu'))
    model.add(Dense(units=30, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    # Fit the model
    model.fit(X_train, y_train, epochs=50, batch_size=5, verbose=1)
    # evaluate model on test data
    y_predict = model.predict(X_test, batch_size=10, verbose=1)
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    print("[INFO] evaluating on testing set...")
    (loss, accuracy) = model.evaluate(X_train, y_train, batch_size=5, verbose=1)
    print("[INFO] loss={:.4f}, accuracy: {:.4f}%".format(loss, accuracy * 100))

    # Save model as json
    model_json = model.to_json()
    with open("diagnosis_penyakit\\neural_network\\model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("diagnosis_penyakit\\neural_network\\model.h5")
    return 'sukses'

def make_data_test():
    data = os.path.join(os.getcwd(), 'diagnosis_penyakit\\neural_network\\dataset.xlsx')
    dataset = pd.read_excel(data, delimiter=',', encoding='latin-1', index_col=False)
    data1 = os.path.join(os.getcwd(), 'diagnosis_penyakit\\neural_network\\databaru.xlsx')
    databaru = pd.read_excel(data1, delimiter=',', encoding='latin-1', index_col=False)
    data5 = []
    X1 = databaru.iloc[:, 0:7].values
    X2 = dataset.iloc[:, 0:7].values
    for i in range(0, len(X1)):
        data5.append(X1[i])
    for i in range(0, len(X2)):
        if X2[i][1]!=X1[0][1]:
            data5.append(X2[i])
    datatest = pd.DataFrame(data5)
    X = datatest.iloc[:, 1:7].values
    # label encoder
    label = LabelEncoder()
    X[:, 0] = label.fit_transform(X[:, 0])
    X[:, 1] = label.fit_transform(X[:, 1])
    # Onehot Encoder
    list = []
    for i in range(0, len(X)):
        if X[i][0] not in list:
            list.append(X[i][0])
    if len(list)==2:
        onehotencoder = OneHotEncoder(categorical_features=[1])
        X = onehotencoder.fit_transform(X).toarray()
    else:
        onehotencoder = OneHotEncoder(categorical_features=[0])
        X = onehotencoder.fit_transform(X).toarray()
        onehotencoder = OneHotEncoder(categorical_features=[len(list)])
        X = onehotencoder.fit_transform(X).toarray()
    datatest_fix = []
    for i in range(0, len(X1)):
        datatest_fix.append(X[i])
    data = pd.DataFrame(datatest_fix)
    data.to_excel('diagnosis_penyakit\\neural_network\\datatestfix.xlsx', index=False)


def makePrediction():
    data = os.path.join(os.getcwd(), 'diagnosis_penyakit\\neural_network\\datatestfix.xlsx')
    datatest = pd.read_excel(data, delimiter=',', encoding='latin-1', index_col=False)
    X = datatest.iloc[:, :].values
    # load model from disk
    json_file = open('diagnosis_penyakit\\neural_network\\model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("diagnosis_penyakit\\neural_network\\model.h5")
    # predict
    y_predict = loaded_model.predict(X, batch_size = 10, verbose=1)
    print (y_predict)
    return y_predict


def updateGejala(y_predict):
    list_pred = []
    gejala = []
    for i in range(0, len(y_predict)):
        if y_predict[i]>0.5:
            list_pred.append(i)
    if len(list_pred)>0:
        data = os.path.join(os.getcwd(), 'diagnosis_penyakit\\neural_network\\datatestfix.xlsx')
        dataset = pd.read_excel(data, delimiter=',', encoding='latin-1', index_col=False)
        dataset = dataset.iloc[:, 0:7].values
        for i in range(0, len(list_pred)):
            for j in range(0, len(dataset)):
                if j==list_pred[i]:
                    gejala.append([dataset[j][0],dataset[j][1],dataset[j][2]])
        for i in range(0, len(gejala)):
            if gejala[i][2]=='Bertambah':
                penyakit = Penyakit.objects.all().filter(nama_penyakit=gejala[i][1])
                gejala2 = Gejala.objects.all().filter(gejala=gejala[i][0], penyakit_id=penyakit[0].id)
                if len(gejala2)==0:
                    updateGejala = Gejala.objects.all().filter(gejala=gejala[i][0])
                    gejala1 = Gejala()
                    gejala1.gejala = gejala[i][0]
                    gejala1.penyakit_id = penyakit[0].id
                    gejala1.id_organ = updateGejala[0].id_organ
                    gejala1.poin = 0
                    gejala1.status = 'Bertambah'
                    gejala1.save()
                    print ('bertambah')

            elif gejala[i][2]=='Berkurang':
                penyakit = Penyakit.objects.all().filter(nama_penyakit=gejala[i][1])
                gejala2 = Gejala.objects.filter(gejala=gejala[i][0], penyakit_id=penyakit[0].id).update(status='Berkurang', poin=0)
        return 'sukses'