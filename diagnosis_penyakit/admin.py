from django.contrib import admin

from .models import Penyakit, Gejala, Pencegahan, Pengobatan, Penyebab, Organ, Diagnosis, UserAnswer, DiagnosisPenyakit, AhliPenyakit, ReviewDiagnosis, ReviewFromUser

admin.site.register(Penyakit)
admin.site.register(Gejala)
admin.site.register(Pencegahan)
admin.site.register(Pengobatan)
admin.site.register(Penyebab)
admin.site.register(Organ)
admin.site.register(Diagnosis)
admin.site.register(UserAnswer)
admin.site.register(DiagnosisPenyakit)
admin.site.register(AhliPenyakit)
admin.site.register(ReviewDiagnosis)
admin.site.register(ReviewFromUser)
