from django.contrib.auth import get_user_model
from .models import Penyakit, UserAnswer
from .models import Gejala
from .models import DiagnosisPenyakit
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

def get_match(gejala_user):
    gejala = list(set(gejala_user))
    penyakit_set = []
    gejala_fix = []
    if(len(gejala_user)>0):
        for i in range(0, len(gejala)):
            gejala_set = Gejala.objects.all()
            for j in range(0, len(gejala_set)):
                k = 0
                space1 = gejala_set[j].gejala.split(' ')
                space2 = gejala[i].split(' ')
                gejala1 = gejala_set[j].gejala.lower().split(str(gejala[i].lower()))
                gejala2 = gejala[i].lower().split(str(gejala_set[j].gejala.lower()))  # gejala inputan user

                hasilMatching = fuzz.ratio(gejala1, gejala2)
                matching = int(hasilMatching)
                # print("gejala 1 : ", gejala1)
                # print("gejala 2 : ", gejala2)
                # print("Hasil matching : ", matching)

                if len(gejala1) == 0 and len(gejala2) == 0:
                    matching == 0

                # if matching == 100:
                #     gejala_fix.append([gejala_set[j], 1])

                if len(space1) == len(space2) and matching == 100:
                    gejala_fix.append([gejala_set[j], 1])
                elif len(space1) != len(space2) and 90 <= matching <= 99:
                    gejala_fix.append([gejala_set[j], 0.8])
                elif len(space1) != len(space2) and 80 <= matching <= 89:
                    gejala_fix.append([gejala_set[j], 0.7])
                elif len(space1) != len(space2) and 70 <= matching <= 79:
                    gejala_fix.append([gejala_set[j], 0.6])
                elif len(space1) != len(space2) and 0 <= matching <= 69:
                    gejala_fix.append([gejala_set[j], 0])

                # print(gejala_fix)
        for j in range(0, len(gejala_fix)):
            gejala2 = gejala_fix[j][0]
            poin = gejala_fix[j][1]
            penyakit_set.append([gejala2.penyakit_id, poin, gejala2.gejala.lower()])
        return penyakit_set

# def get_match(gejala_user):
#     gejala = list(set(gejala_user))
#     penyakit_set = []
#     gejala_fix = []
#     # test = fuzz.ratio("Nyeri", "Nyeri Tekan")
#     # print("Hasil matching : ", test)
#     if(len(gejala_user)>0):
#         for i in range(0, len(gejala)):
#             gejala_set = Gejala.objects.all()
#             for j in range(0, len(gejala_set)):
#                 k = 0
#                 space1 = gejala_set[j].gejala.split(' ') #untuk memisahkan kata dari gejala1
#                 space2 = gejala[i].split(' ') #untuk memisahkan kata dari gejala2
#                 gejala1 = gejala_set[j].gejala.lower().split(str(gejala[i].lower())) # gejala dari database
#                 gejala2 = gejala[i].lower().split(str(gejala_set[j].gejala.lower())) # gejala inputan user
#
#                 # print("gejala database : ", gejala_set[j])
#                 # print("gejala user : ", gejala[i])
#
#                 hasilMatching = fuzz.ratio(gejala1, gejala2)
#                 matching = int(hasilMatching)
#                 print("gejala 1 : ", gejala1)
#                 print("gejala 2 : ", gejala2)
#                 print("Hasil matching : ", matching)
#
#                 if len(gejala1) == len(gejala2) and matching == 100:
#                     gejala_fix.append([gejala_set[j], 1])
#                 elif 80 <= matching <= 99:
#                     gejala_fix.append([gejala_set[j], 0.8])
#                 elif 70 <= matching <= 79:
#                     gejala_fix.append([gejala_set[j], 0.7])
#                 elif 60 <= matching <= 69:
#                     gejala_fix.append([gejala_set[j], 0.6])
#                 elif 0 <= matching <= 59:
#                     gejala_fix.append([gejala_set[j], 0])
#             # print(gejala_fix)
#         for j in range(0, len(gejala_fix)):
#             gejala2 = gejala_fix[j][0]
#             poin = gejala_fix[j][1]
#             penyakit_set.append([gejala2.penyakit_id, poin, gejala2.gejala.lower()])
#             return(penyakit_set)
#         return penyakit_set

def makePoint(penyakit_set):
    if(len(penyakit_set)>0):
        penyakit_gejala = []
        penyakit_sama = []
        point = 0
        list_point = []
        penyakit_poin = []
        fix_penyakit_point = []
        penyakit = 0
        for i in range(0, len(penyakit_set)):
            penyakit_sama.append(penyakit_set[i])

        for i in range(0, len(penyakit_set)):
            for j in range(0, len(penyakit_set)):
                if(penyakit_sama[i][0] == penyakit_set[j][0]):
                    penyakit = penyakit_sama[i][0]
                    point += penyakit_set[j][1]
            penyakit_gejala.append([penyakit, point])
            point = 0

        for i in range(0, len(penyakit_gejala)):
            if penyakit_gejala[i] not in penyakit_poin:
                penyakit_poin.append(penyakit_gejala[i])
                list_point.append(penyakit_gejala[i][1])
        list_point = sorted(list_point,reverse=True)
        for i in range(0, len(list_point[:10])):
            for j in range(0, len(penyakit_poin)):
                if list_point[i] == penyakit_poin[j][1]:
                    if penyakit_poin[j] not in fix_penyakit_point:
                        fix_penyakit_point.append(penyakit_poin[j])
        print (fix_penyakit_point)
        return fix_penyakit_point[:10]
    return penyakit_set

def save_match(user, penyakit_gejala):
    num_diagnosis = UserAnswer.objects.all().filter(user=user).order_by('-number_diagnosis')
    number = num_diagnosis[0]

    for i in range(0, len(penyakit_gejala)):
        diagnosis = DiagnosisPenyakit.objects.all().filter(user=user, number_diagnosis=number.number_diagnosis, penyakit_id=penyakit_gejala[i][0])
        if len(diagnosis)==0:
            diagnosisPenyakit = DiagnosisPenyakit()
            diagnosisPenyakit.user = user
            diagnosisPenyakit.penyakit_id = penyakit_gejala[i][0]
            diagnosisPenyakit.total_point = penyakit_gejala[i][1]
            diagnosisPenyakit.number_diagnosis = number.number_diagnosis
            diagnosisPenyakit.save()

    return 'sukses'
