from django.db import models
from django.urls import reverse
from django.conf import settings
from utama.models import Administrator, Akun

# Create your models here.
class Penyakit(models.Model):
    BIDANG_CHOICES = (
        ('dokter umum','Dokter Umum'),
        ('dokter paru','Dokter Paru'),
        ('dokter bedah', 'Dokter Bedah'),
        ('dokter saraf ', 'Dokter Paru'),
        ('dokter jantung ', 'Dokter Jantung'),
        ('dokter kandungan', 'Dokter Kandungan'),
        ('dokter mata', 'Dokter Mata'),
        ('dokter penyakit dalam', 'Dokter Penyakit Dalam'),
        ('dokter THT ', 'Dokter THT'),
        ('Dokter Saraf', 'Dokter Saraf'),
        ('dokter onkologi', 'Dokter Onkologi'),
        ('dokter anestesi', 'Dokter Anestesi')
    )
    nama_penyakit = models.CharField(max_length=100)
    definisi = models.TextField(null=True, blank=True)
    bidang_ahli = models.CharField(max_length=50, choices=BIDANG_CHOICES, default='Dokter Umum')

    def get_absolute_url(self):
        return reverse('penyakit:penyakit_detail', kwargs={'pk':self.pk})
    def __str__(self):
        return self.nama_penyakit

class Gejala(models.Model):
    penyakit_id = models.IntegerField()
    gejala = models.CharField(max_length=150)
    id_organ = models.IntegerField(null=False)
    status = models.CharField(max_length=150, default='Tetap')
    def get_absolute_url(self):
        return reverse('gejala:gejala_detail', kwargs={'pk':self.pk})
    def __str__(self):
        return self.gejala

class Pencegahan(models.Model):
    penyakit_id = models.IntegerField()
    pencegahan = models.TextField(null=True, blank=True)

    def get_absolute_url(self):
        return reverse('pencegahan:pencegahan_detail', kwargs={'pk':self.pk})
    def __str__(self):
        return self.pencegahan


class Pengobatan(models.Model):
    penyakit_id = models.IntegerField()
    pengobatan = models.TextField(null=True, blank=True)

    def get_absolute_url(self):
        return reverse('pengobatan:pengobatan_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.pengobatan

class Penyebab(models.Model):
    penyakit_id = models.IntegerField()
    penyebab = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.penyebab

class Diagnosis(models.Model):
    penyakit_id = models.IntegerField()
    diagnosis = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.diagnosis

    def get_absolute_url(self):
        return reverse('diagnosis:diagnosis_detail', kwargs={'pk': self.pk})

class Organ(models.Model):
    organ = models.CharField(max_length=150)

    def __str__(self):
        return self.organ


class UserAnswer(models.Model):
    gejala_answer = models.CharField(max_length=250)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    number_diagnosis = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.gejala_answer

class DiagnosisPenyakit(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    penyakit = models.ForeignKey(Penyakit, on_delete=models.CASCADE)
    number_diagnosis = models.IntegerField(default=0)
    total_point = models.IntegerField(blank=False)
    status_learning = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

class ReviewFromUser(models.Model):
    BIDANG_CHOICES = (
        ('Dokter Umum', 'Dokter Umum'),
        ('Dokter Paru', 'Dokter Paru'),
        ('Dokter Bedah', 'Dokter Bedah'),
        ('Dokter Paru', 'Dokter Paru'),
        ('Dokter Jantung', 'Dokter Jantung'),
        ('Dokter Kandungan', 'Dokter Kandungan'),
        ('Dokter Mata', 'Dokter Mata'),
        ('Dokter Penyakit Dalam', 'Dokter Penyakit Dalam'),
        ('Dokter THT', 'Dokter THT'),
        ('Dokter Saraf', 'Dokter Saraf'),
        ('Dokter Onkologi', 'Dokter Onkologi'),
        ('Dokter Anestesi', 'Dokter Anestesi'),
        ('Dokter Anak', 'Dokter Anak'),
        ('Dokter Gigi', 'Dokter Gigi'),
        ('Dokter Kulit', 'Dokter Kulit'),
        ('Dokter Gastroenterologi', 'Dokter Gastroenterologi'),
        ('Dokter Jiwa', 'Dokter Jiwa'),
        ('Dokter Ortopedi', 'Dokter Ortopedi'),
        ('Dokter Radiologi', 'Dokter Radiologi'),
        ('Dokter Reumatologi', 'Dokter Reumatologi'),
        ('Dokter Bedah Anak', 'Dokter Bedah Anak'),
        ('Dokter Urologi', 'Dokter Urologi'),
        ('Ahli Gizi', 'Ahli Gizi'),
        ('Dokter Endokrin', 'Dokter Endokrin'),
        ('Dokter Ginjal', 'Dokter Ginjal')
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    number_diagnosis = models.IntegerField(default=0)
    nama_dokter = models.CharField(max_length=250)
    ahli_bidang = models.CharField(max_length=50, choices=BIDANG_CHOICES, default='Dokter Umum')
    lama_kerja = models.IntegerField(default=0)
    penyakit_id = models.IntegerField(null=False)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nama_dokter


class AhliPenyakit(models.Model):
    BIDANG_CHOICES = (
        ('Dokter Umum','Dokter Umum'),
        ('Dokter Paru','Dokter Paru'),
        ('Dokter Bedah', 'Dokter Bedah'),
        ('Dokter Paru', 'Dokter Paru'),
        ('Dokter Jantung', 'Dokter Jantung'),
        ('Dokter Kandungan', 'Dokter Kandungan'),
        ('Dokter Mata', 'Dokter Mata'),
        ('Dokter Penyakit Dalam', 'Dokter Penyakit Dalam'),
        ('Dokter THT', 'Dokter THT'),
        ('Dokter Saraf', 'Dokter Saraf'),
        ('Dokter Onkologi', 'Dokter Onkologi'),
        ('Dokter Anestesi', 'Dokter Anestesi'),
        ('Dokter Anak', 'Dokter Anak'),
        ('Dokter Gigi', 'Dokter Gigi'),
        ('Dokter Kulit', 'Dokter Kulit'),
        ('Dokter Gastroenterologi', 'Dokter Gastroenterologi'),
        ('Dokter Jiwa', 'Dokter Jiwa'),
        ('Dokter Ortopedi', 'Dokter Ortopedi'),
        ('Dokter Radiologi', 'Dokter Radiologi'),
        ('Dokter Reumatologi', 'Dokter Reumatologi'),
        ('Dokter Bedah Anak', 'Dokter Bedah Anak'),
        ('Dokter Urologi', 'Dokter Urologi'),
        ('Ahli Gizi', 'Ahli Gizi'),
        ('Dokter Endokrin', 'Dokter Endokrin'),
        ('Dokter Ginjal', 'Dokter Ginjal')
    )

    nip = models.CharField(max_length=25)
    nama = models.CharField(max_length=250)
    akun = models.ForeignKey(Akun, related_name='dokters', on_delete=models.CASCADE)
    alamat = models.CharField(max_length=350)
    nomor_telepon = models.CharField(max_length=15)
    ahli_bidang = models.CharField(max_length=50, choices=BIDANG_CHOICES, default='Dokter Umum')
    lama_kerja = models.IntegerField(default=0)
    jumlah_review_benar = models.IntegerField(default=0, blank=True)
    jumlah_review = models.IntegerField(default=0, blank=True)
    foto_profil = models.ImageField(upload_to='foto_profil_dokter', blank=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    tempat_kerja = models.CharField(max_length=500)
    administrator = models.ForeignKey(Administrator, blank=True, null=True, default=1, related_name='dokters', on_delete=models.CASCADE)

    def __str__(self):
        return self.nama

class ReviewDiagnosis(models.Model):
    ahli = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    user = models.IntegerField()
    penyakit_id = models.IntegerField(null=False)
    number_diagnosis = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    def __str__(self):
        return self.ahli
